<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>OOP learn</title>
  </head>
  <body>
    <?php

      require_once 'animal.php';
      require_once 'frog.php';
      require_once 'ape.php';

      $sheep = new animal("shaun", 2, "false");
      $kodok = new frog("buduk", 4, "true");
      $sungokong = new ape("kera sakti", 2,"false");
    


      echo $sheep->name; // "shaun"
      echo "<br>";
      echo $sheep->legs; // 2
      echo "<br>";
      echo $sheep->cold_blood; // false
      echo "<br>";
      echo "<br>";
      echo "<br>";
      echo $kodok->name;
      echo "<br>";
      echo $kodok->legs;
      echo "<br>";
      echo $kodok->cold_blood;
      echo "<br>";
      echo $kodok->jump();
      echo "<br>";
      echo "<br>";
      echo "<br>";
      echo $sungokong->name;
      echo "<br>";
      echo $sungokong->legs;
      echo "<br>";
      echo $sungokong->cold_blood;
      echo "<br>";
      echo $sungokong->yell();




      // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())







    ?>
  </body>
</html>
